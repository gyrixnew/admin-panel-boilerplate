import React from 'react';
import { Typography } from '@material-ui/core';

export default () => {
    return (
        <div className="d-flex flex-column justify-content-center align-items-center mt-5">
            <Typography component="h1" variant="h5">Dashboard</Typography>
        </div >
    );
}